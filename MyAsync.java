public class MyAsync extends Thread {
	private volatile boolean done = false;

	public void run() {
		while (!done) {//子线程中的循环
			System.out.println("thread out x");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
//				Thread.currentThread().interrupt();
			}
		}
	}

	public synchronized void setDone() {
		done = true;
	}

	public static void main(String argv[]) {
		MyAsync t = new MyAsync();
		t.start();//起子线程
		long last = System.currentTimeMillis();
		int count = 0;
		while(true) {//主线程中循环
			long now = System.currentTimeMillis();
			if (now - last > 1000 * 1) {
				last = now;
				count ++;
				System.out.println("the " + count + "th count.");
			}
			if (count > 2) {
				break;
			}
			
		}

		t.setDone();
	}
}

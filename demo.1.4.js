var synchronizedCode = require('./sync');
(function() {
	setTimeout(function() {synchronizedCode('timer1',3);},0);
	setTimeout(function() {console.log('setTimeout 0 occured second.');},0);
	process.nextTick(function() {console.log('nextTick occured.');});
	
	var addon = require('./addon/build/Release/binding');
	addon.async_hello("good",function(err, result) {
		console.log('node addon result',result);
	});
	
	setImmediate(function() {console.log('setImmediate occured.')});
	
	var fs = require('fs');
	var crypto = require('crypto');
	var rand = crypto.pseudoRandomBytes(8).toString('hex');
	fs.mkdir('d:\\temp\\xx'+'\\'+rand,function(err) {
		if (err) {
			console.log(err,'错误',err.code);
		} else {
			console.log('create directory success.');
		}		
	});
	
	synchronizedCode();
})();
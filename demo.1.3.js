var synchronizedCode = require('./sync');
(function() {
	setTimeout(function() {console.log('setTimeout 0 occured first.');},0);
	setTimeout(function() {console.log('setTimeout 0 occured second.');},0);
	process.nextTick(function() {console.log('nextTick occured.');});
	setImmediate(function() {console.log('setImmediate occured.')});
	
	var fs = require('fs');
	var crypto = require('crypto');
	var rand = crypto.pseudoRandomBytes(8).toString('hex');
	fs.mkdir('d:\\temp\\xx'+'\\'+rand,function(err) {
		if (err) {
			console.log(err,'错误',err.code);
		} else {
			console.log('create directory success.');
		}		
	});
	
	synchronizedCode();
})();
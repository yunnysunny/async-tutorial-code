{
  'targets': [
    {
      'target_name': 'binding',
	  'defines': [
        'DEFINE_FOO',
        'DEFINE_A_VALUE=value',
      ],
	  'conditions' : [
			['OS=="linux"', {
			  'defines': [
				'LINUX_DEFINE',
			  ],
			  
			  'libraries':[
                  '-lpthread'
			  ],
			  'sources': [ 'src/hello2.cc' ]
			}],
			['OS=="win"', {
			  'defines': [
				'WINDOWS_SPECIFIC_DEFINE',
			  ],
			  'sources': [ 'src/hello2.cc' ]
			}]
		]
      
    }
  ]
}
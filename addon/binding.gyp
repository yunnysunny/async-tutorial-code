{
  'targets': [
    {
      'target_name': 'binding',
	  'defines': [
        'DEFINE_FOO',
        'DEFINE_A_VALUE=value',
      ],
	  "include_dirs" : [
			"<!(node -e \"require('nan')\")"
	  ],
	  'conditions' : [
			['OS=="linux"', {
			  'defines': [
				'LINUX_DEFINE',
			  ],
			  
			  'libraries':[
                  '-lpthread'
			  ],
			  'sources': [ 'src/hello3.cc' ]
			}],
			['OS=="win"', {
			  'defines': [
				'WINDOWS_SPECIFIC_DEFINE',
			  ],
			  'sources': [ 'src/hello3.cc' ]
			}]
		]
      
    }
  ]
}
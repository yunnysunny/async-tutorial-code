function synchronizedCode(tag, times) {
	tag = tag || '';
	times = times || 4;
	var last = new Date().getTime();
	var count = 0;
	while (true) {
		var now = new Date().getTime();
		if (now - last > 1000 * 2) {
			last = now;
			count++;
			if (tag) {
				console.log('the %dth count in [%s].',count,tag);
			} else {
				console.log('the %dth count.',count);
			}
		}
		if (count > times) {
			if (tag) {
				console.log('exist while in [%s].',tag);
			} else {
				console.log('exist while.');
			}
			break;
		}
	}
}

module.exports = synchronizedCode;